import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

export default class AboutModal extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <TouchableOpacity
        style={{marginRight: 10}}
        onPress={() => navigation.navigate('Modal')}>
        <Text style={{color: '#222'}}> Hakkımda </Text>
      </TouchableOpacity>
    );
  }
}
