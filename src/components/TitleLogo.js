import React from 'react';
import {Thumbnail} from 'native-base';

export default class TitleLogo extends React.Component {
  render() {
    return <Thumbnail square small source={require('../../assets/logo.jpg')} />;
  }
}
