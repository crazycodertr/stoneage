import React from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {View} from 'native-base';
import PlacesItem from './PlacesItem';

export default class Index extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.places}
          keyExtractor={(item, key) => item.id.toString()}
          horizontal={true}
          renderItem={({item}) => (
            <PlacesItem map={this.props.map} item={item} />
          )}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 150,
    padding: 10,
  },
});
