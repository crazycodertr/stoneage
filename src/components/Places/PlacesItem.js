import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import {API_KEY, API_ENDPOINT} from '../../../constants';

export default class PlacesItem extends React.Component {
  onPress = () => {
    const {lat, lng} = this.props.item.geometry.location;
    const newRegion = {
      latitude: lat,
      longitude: lng,
      latitudeDelta: 0.002,
      longitudeDelta: 0.002,
    };

    this.props.map.animateToRegion(newRegion, 1000);
  };
  render() {
    const {photos} = this.props.item;
    let source;
    if (photos) {
      source = {
        uri: `${API_ENDPOINT}/photo?maxwidth=400&photoreference=${
          photos[0].photo_reference
        }&key=${API_KEY}`,
      };
    }
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View style={styles.itemContainer}>
          <Text numberOfLines={1} style={styles.title}>
            {this.props.item.name}
          </Text>
          {source ? (
            <Image source={source} style={styles.photo} />
          ) : (
            <Text numberOfLines={1}>{this.props.item.name}</Text>
          )}
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    width: 220,
    height: 150,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ddd',
    marginRight: 5,
  },
  photo: {
    width: '110%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
  },
  title: {
    padding: 5,
    backgroundColor: '#fff',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 2,
  },
});
