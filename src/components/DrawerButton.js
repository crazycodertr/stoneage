import React from 'react';
import {Icon} from 'native-base';
import {TouchableOpacity} from 'react-native';

export default class DrawerButton extends React.Component {
  toggleMenu = () => {
    this.props.navigation.toggleDrawer();
  };
  render() {
    return (
      <TouchableOpacity onPress={this.toggleMenu} style={{padding: 10}}>
        <Icon style={{color: '#000'}} name="menu" size={25} />
      </TouchableOpacity>
    );
  }
}
