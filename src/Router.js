import React from 'react';

import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import {Icon} from 'native-base';

// Screens
import Home from './Screens/Home';
import Map from './Screens/Maps';
import AboutScreen from './Screens/AboutModal';

import DrawerIcon from './components/DrawerButton';
import TitleLogo from './components/TitleLogo';
import AboutModal from './components/AboutModal';

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({navigation}) => ({
        headerLeft: <DrawerIcon navigation={navigation} />,
        headerTitle: <TitleLogo />,
        headerRight: <AboutModal navigation={navigation} />,
      }),
    },
    Map: {
      screen: Map,
      navigationOptions: ({navigation}) => ({
        headerLeft: <DrawerIcon navigation={navigation} />,
      }),
    },
  },
  {
    headerLayoutPreset: 'center',
  },
);

const MapStack = createStackNavigator({
  Map: {
    screen: Map,
    navigationOptions: ({navigation}) => ({
      headerLeft: <DrawerIcon navigation={navigation} />,
    }),
  },
});

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        drawerLabel: 'Anasayfa',
        drawerIcon: ({tintColor}) => (
          <Icon name="home" size={20} style={{color: tintColor}} />
        ),
      },
    },
    Map: {
      screen: MapStack,
      navigationOptions: {
        drawerLabel: 'Harita',
        drawerIcon: ({tintColor}) => (
          <Icon name="map" size={20} style={{color: tintColor}} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Home',
    drawerWidth: 200,
    contentOptions: {
      activeTintColor: '#f44',
      inactiveTintColor: '#000',
      style: {
        backgroundColor: '#171400',
      },
    },
  },
);

const ModalStack = createStackNavigator(
  {
    Main: DrawerNavigator,
    Modal: AboutScreen,
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

export default createAppContainer(ModalStack);
