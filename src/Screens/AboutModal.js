import React, {Component} from 'react';
import {Text, SafeAreaView, StyleSheet} from 'react-native';
import {Thumbnail} from 'native-base';
import {Linking} from 'react-native';

export default class AboutModal extends Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Thumbnail
          style={styles.profile}
          source={require('../../assets/profile.png')}
        />
        <Thumbnail
          style={styles.img}
          square
          source={require('../../assets/chronobi.png')}
        />
        <Text style={styles.text}> Mesut KILINCASLAN </Text>
        <Text style={styles.desc}> Chronobi Kurucu Eş Başkan </Text>
        <Text
          onPress={() => Linking.openURL('https://crazycoder.io')}
          style={styles.link}>
          {' '}
          crazycoder.io{' '}
        </Text>
        <Thumbnail
          style={styles.sign}
          square
          source={require('../../assets/sign1.png')}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  sign: {
    // flex: 0.8,
    width: 120,
    height: 60,
    // marginTop: 10,
    alignSelf: 'flex-end',
    marginRight: 5,
    marginBottom: 10,
    transform: [{rotate: '-20deg'}],
  },
  profile: {
    marginTop: 5,
    width: 200,
    height: 200,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#000',
  },
  img: {width: 300, height: 300},
  desc: {
    marginTop: 10,
    fontWeight: '600',
  },
  link: {
    flex: 1,
    // marginLeft: 120,
    marginTop: 10,
    textDecorationLine: 'underline',
  },
});
