import React, {Component} from 'react';
import {StyleSheet, SafeAreaView, View} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

import {Spinner} from 'native-base';

import axios from 'axios';

import Permissions from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';

import {API_ENDPOINT, API_KEY} from '../../../constants';

import Places from '../../components/Places';

class Index extends Component {
  constructor(props) {
    super(props);
    this.url = props.navigation.getParam('url');
    this.distance = props.navigation.getParam('distance');
    console.log(this.distance);
  }

  state = {
    region: {
      latitude: 41.0087,
      longitude: 29.0173,
      latitudeDelta: 0.05,
      longitudeDelta: 0.0421,
    },
    places: [],
    fetching: false,
  };

  async componentDidMount() {
    const permissions = await Permissions.request(
      'android.permission.ACCESS_FINE_LOCATION',
    );
    if (permissions !== 'granted') {
      alert('Please open location permissions');
      return false;
    }

    try {
      const {coords} = await this.getCurrentPosition();
      this.setState({fetching: true});
      const {
        data: {results},
      } = await axios.get(
        `${API_ENDPOINT}/nearbysearch/json?location=${coords.latitude}, ${
          coords.longitude
        }&radius=${this.distance}&type=${this.url}&key=${API_KEY}`,
      );

      this.setState({
        region: {
          ...this.state.region,
          latitude: coords.latitude,
          longitude: coords.longitude,
        },
        fetching: false,
        places: results,
      });
    } catch (e) {
      this.setState({fetching: false});
      alert('Could not get location');
    }
  }

  getCurrentPosition() {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition(position => resolve(position), reject, {
        enableHighAccuracy: false, //It means read from GPS, false means read from wifi
        timeout: 5000,
        // maximumAge: 0 //It means read location every second
      });
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <MapView
          loadingEnabled={true}
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={this.state.region}
          ref={ref => (this.map = ref)}>
          {this.state.places.map(place => {
            const {
              geometry: {
                location: {lat, lng},
              },
            } = place;

            return (
              <Marker
                key={place.id}
                title={place.name}
                coordinate={{
                  latitude: lat,
                  longitude: lng,
                }}
                image={{
                  uri:
                    'https://c7.uihere.com/icons/918/396/965/direction-favorite-location-map-map-marker-star-icon-96ce7ac3ecddaa357846a52c3c3bb6fa.png',
                }}
              />
            );
          })}
        </MapView>
        <View style={styles.placesContainer}>
          {this.state.fetching ? (
            <Spinner />
          ) : (
            <Places map={this.map} places={this.state.places} />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
  },
  placesContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
  },
});

export default Index;
