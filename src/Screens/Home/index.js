import React from 'react';
import {StyleSheet, FlatList, TextInput, TouchableOpacity} from 'react-native';
import {View, Container, Text, Icon, Picker, Item} from 'native-base';

import SplashScreen from 'react-native-splash-screen';
/**
 * PlaceTypes
 */
import placeTypes from '../../lib/PlaceTypes';

class Home extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  state = {
    text: '',
    data: placeTypes,
    selected2: '1000',
  };

  renderContent = place_type => {
    const {name, icon, color, url} = place_type.item;
    return (
      <View>
        <TouchableOpacity
          key={url}
          onPress={() =>
            this.props.navigation.navigate('Map', {
              url,
              distance: this.state.selected2,
            })
          }
          iconLeft
          rounded
          style={[styles.locations, {backgroundColor: color}]}>
          <Icon name={icon} />
          <Text style={styles.placeName}>{name}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  searchFilter = text => {
    const newData = placeTypes.filter(item => {
      const listText = `${item.name.toLocaleLowerCase()}`;

      return listText.indexOf(text.toLowerCase()) > -1;
    });

    this.setState({
      data: newData,
    });
  };
  renderHeader = () => {
    const {text} = this.state;
    return (
      <View style={styles.searchContainer}>
        <TextInput
          onChangeText={text => {
            this.setState({
              text,
            });

            this.searchFilter(text);
          }}
          value={text}
          placeholder={'Ara'}
          style={styles.searchInput}
        />

        <Item picker>
          <Picker
            mode="dropdown"
            Icon={<Icon name="arrow-down" />}
            iosHeader="Mesafe"
            style={styles.distance}
            placeholder="Mesafe"
            placeholderStyle={{color: '#000'}}
            placeholderIconColor="#007aff"
            itemStyle={styles.distanceItem}
            selectedValue={this.state.selected2}
            onValueChange={this.onValueChange2.bind(this)}>
            <Picker.Item
              style={styles.distanceItem}
              label="1 km"
              value="1000"
            />
            <Picker.Item
              style={styles.distanceItem}
              label="3 km"
              value="3000"
            />
            <Picker.Item
              style={styles.distanceItem}
              label="5 km"
              value="5000"
            />
            <Picker.Item
              style={styles.distanceItem}
              label="7 km"
              value="7000"
            />
            <Picker.Item
              style={styles.distanceItem}
              label="10 km"
              value="10000"
            />
          </Picker>
        </Item>
      </View>
    );
  };
  onValueChange2(value) {
    this.setState({
      selected2: value,
    });
  }
  render() {
    return (
      <Container style={styles.main_container}>
        <View style={styles.content}>
          <FlatList
            ListHeaderComponent={this.renderHeader}
            renderItem={this.renderContent}
            keyExtractor={item => item.url}
            data={this.state.data}
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#dfdfdf',
  },
  distance: {
    marginTop: 15,
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    // flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 15,
    padding: 5,
  },
  locations: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 25,
    marginTop: 5,
    justifyContent: 'center',
  },
  distanceItem: {
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
  },
  placeName: {
    fontSize: 16,
    marginTop: 5,
    marginLeft: 5,
  },
  searchContainer: {
    padding: 10,
  },
  searchInput: {
    textAlign: 'center',
    borderRadius: 25,
    fontSize: 16,
    backgroundColor: '#f9f9f9',
    padding: 10,
  },
});

export default Home;
