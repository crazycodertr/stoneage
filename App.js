import React, {Component} from 'react';
import Router from './src/Router';
import NavigationService from './src/NavigationService';

class App extends Component {
  render() {
    return (
      <Router
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

export default App;
